//
//  ViewController.swift
//  Calc50
//
//  Created by Администратор on 11.11.2019.
//  Copyright © 2019 Sergey Klimovich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var resultLabel: UILabel!
    
    var numberOne = ""
    var numberTwo = ""
    var move = ""
    var result = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func action(_ sender: UIButton) {
        let button = sender.accessibilityIdentifier
        
        switch button {
        case "0":
            if move == "" {
                if numberOne != "0" {
                    numberOne = numberOne + "0"
                    resultLabel.text = numberOne
                }
            } else {
                if numberTwo != "0" {
                    numberTwo = numberTwo + "0"
                    resultLabel.text = numberTwo
                }
            }
        case "1":
            if move == "" {
                numberOne = numberOne + "1"
                resultLabel.text = numberOne
            } else {
                numberTwo = numberTwo + "1"
                resultLabel.text = numberTwo
            }
        case "2":
            if move == "" {
                numberOne = numberOne + "2"
                resultLabel.text = numberOne
            } else {
                numberTwo = numberTwo + "2"
                resultLabel.text = numberTwo
            }
        case "3":
            if move == "" {
                numberOne = numberOne + "3"
                resultLabel.text = numberOne
            } else {
                numberTwo = numberTwo + "3"
                resultLabel.text = numberTwo
            }
        case "4":
            if move == "" {
                numberOne = numberOne + "4"
                resultLabel.text = numberOne
            } else {
                numberTwo = numberTwo + "4"
                resultLabel.text = numberTwo
            }
        case "5":
            if move == "" {
                numberOne = numberOne + "5"
                resultLabel.text = numberOne
            } else {
                numberTwo = numberTwo + "5"
                resultLabel.text = numberTwo
            }
        case "6":
            if move == "" {
                numberOne = numberOne + "6"
                resultLabel.text = numberOne
            } else {
                numberTwo = numberTwo + "6"
                resultLabel.text = numberTwo
            }
        case "7":
            if move == "" {
                numberOne = numberOne + "7"
                resultLabel.text = numberOne
            } else {
                numberTwo = numberTwo + "7"
                resultLabel.text = numberTwo
            }
        case "8":
            if move == "" {
                numberOne = numberOne + "8"
                resultLabel.text = numberOne
            } else {
                numberTwo = numberTwo + "8"
                resultLabel.text = numberTwo
            }
        case "9":
            if move == "" {
                numberOne = numberOne + "9"
                resultLabel.text = numberOne
            } else {
                numberTwo = numberTwo + "9"
                resultLabel.text = numberTwo
            }
        case "ac":
            numberOne = ""
            numberTwo = ""
            move = ""
            resultLabel.text = "0"
        case "plus":
            move = "plus"
        case "delenie":
            move = "delenie"
        case "*":
            move = "*"
        case "minus":
            move = "minus"
        case "plusMinus":
            if resultLabel.text != "0" {
                if move == "" {
                    numberOne = String(-1 * Double(numberOne)!)
                    if Double(numberOne)!.truncatingRemainder(dividingBy: 1) != 0 {
                        resultLabel.text = String(numberOne)
                    } else {
                        if let hdh = Int(numberOne) {
                        resultLabel.text = String(hdh)
                        }
                    }
                } else {
                    numberTwo = String(-1 * Double(numberTwo)!)
                    if Double(numberTwo)!.truncatingRemainder(dividingBy: 1) != 0 {
                        resultLabel.text = String(numberTwo)
                    } else {
                        resultLabel.text = String(Int(numberTwo)!)
                    }
                }
            }
        case "result":
            resultAction()
        default:
            break
        }
    }
    
    //Без обид
    func resultAction() {
        var temp = 0.0
        switch move {
        case "plus":
            temp = Double(numberOne)! + Double(numberTwo)!
        case "delenie":
            if Double(numberTwo)! != 0.0 {
                temp = Double(numberOne)! / Double(numberTwo)!
            }
        case "minus":
            temp = Double(numberOne)! - Double(numberTwo)!
        case "*":
            temp = Double(numberOne)! * Double(numberTwo)!
        default:
            break
        }
        if temp.truncatingRemainder(dividingBy: 1) != 0 {
            result = String(temp)
        } else {
            result = String(Int(temp))
        }
        resultLabel.text = result
        numberOne = result
        numberTwo = ""
        move = ""
    }
}

